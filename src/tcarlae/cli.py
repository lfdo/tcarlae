# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: Copyright 2023 David Seaward and contributors


def invoke():
    print("Hello world!")


if __name__ == "__main__":
    invoke()
